#!/usr/bin/env bash

echo "Initializing my project..."

chsh -s /bin/bash www-data && composer install --prefer-dist -v;

php bin/console -n doctrine:migrations:migrate
php bin/console -n doctrine:fixtures:load

echo "Done!"
