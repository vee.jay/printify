# Printify API

### Install before
1. The command line `git`
2. Docker

### Run application
1. Run in your working directory: `git clone https://gitlab.com/vee.jay/printify.git`
2. Install Docker
3. Run application `docker-compose up --build`
    - Symfony application will be installed in the container named `printify_api_1`
    - Migrations will be executed
    - Database will be purged
    - Database will be seeded with default data (mostly for tests)
    - phpMyAdmin container will be up and running to check database data and structure

### Access
1. Application will be run on port 8000. http://localhost:8000/
2. phpMyAdmin will be available on port 8001. http://localhost:8001/
    - user: root
    - password: root
    
### Testing application
1. SSH into application container, named printify_api_1: `docker exec -it printify_api_1 /bin/bash`
2. Run `bin/phpunit`
    - On first run `PhpUnit` will be installed

#### Still missing / Not clear / TODO
1. Full CRUD functionality for models
2. Tests is not covering correct validation for the deducted sum
3. Price of the product could be in the database, also could be categorised by product type
4. Prices of the shipping also can be in the database
5. Countries list must be in database and have relation to user
6. Tests can be successfully executed only on first run of the container as database is not truncating keys before seeding
