FROM php:7.2-apache

RUN docker-php-ext-install mysqli pdo_mysql

RUN a2enmod rewrite

RUN apt-get update \
    && apt-get install -y zip \
    && apt-get install -y unzip

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY --from=composer /usr/bin/composer /usr/local/bin/composer
COPY composer.json composer.lock ./

# set right apache public folder
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

WORKDIR /var/www/html/

CMD sh scripts/run.sh && apache2-foreground
#EXPOSE 80
