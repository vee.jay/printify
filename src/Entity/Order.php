<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="orders")
     */
    private $product;

    /**
     * @Assert\Choice({"new", "processing", "finished"}, message = "The field status must be defined as 'new', 'processing' or 'finished'")
     * @ORM\Column(type="string", columnDefinition="ENUM('new', 'processing', 'finished')")
     */
    private $status;

    /**
     * @Assert\NotBlank(message = "The full_name field can not be empty.")
     * @ORM\Column(type="string", length=255)
     */
    private $full_name;

    /**
     * @Assert\Choice({"World", "US"}, message = "The field status must be defined as 'World' or 'US'")
     * @ORM\Column(type="string", columnDefinition="ENUM('World', 'US')")
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip;

    /**
     * @Assert\NotBlank(message = "The phone field can not be empty.")
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="smallint")
     */
    private $express_shipping;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $cost;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    public function setFullName(string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getExpressShipping(): ?int
    {
        return $this->express_shipping;
    }

    public function setExpressShipping(int $express_shipping): self
    {
        $this->express_shipping = $express_shipping;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCost(): ?string
    {
        return $this->cost;
    }

    public function setCost(string $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function toArray(): array
    {
        $user = $this->getUser();
        $products = $this->getProduct();

        $productList = [];
        foreach ($products as $product) {
            $productList[] = $product->toArray();
        }

        return [
            'id' => $this->getId(),
            'user' => [
                'id' => $user->getId(),
                'full_name' => $user->getFullName(),
                'balance' => $user->getBalance()
            ],
            'products' => $productList,
            'full_name' => $this->getFullName(),
            'country' => $this->getCountry(),
            'address' => $this->getAddress(),
            'street' => $this->getStreet(),
            'state' => $this->getState(),
            'phone' => $this->getPhone(),
            'status' => $this->getStatus(),
            'express' => (bool)$this->getExpressShipping(),
            'cost' => $this->getCost()
        ];
    }
}
