<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setFullName($faker->firstName . ' ' . $faker->lastName);
            $user->setEmail($faker->email);
            $user->setBalance(($i == 1) ? 5 : 100);
            $manager->persist($user);

            // add 2 products for 3 random users
            if (in_array($i, [1, 5, 8])) {
                for ($cnt = 0; $cnt < 2; $cnt++) {
                    $color = $faker->colorName;

                    $product = new Product();
                    $product->setTitle('My Mug ' . $color);
                    $product->setCost(3.55);
                    $product->setSku('MUG-' . strtoupper($color) . '-' . $faker->randomNumber(5));
                    $product->setType('mug');
                    $product->setUser($user);
                    $manager->persist($product);
                    $manager->flush();

                    $product = new Product();
                    $product->setTitle('My T-Shirt ' . $color);
                    $product->setCost(2.30);
                    $product->setSku('TS-' . strtoupper($color) . '-' . $faker->randomNumber(5));
                    $product->setType('tshirt');
                    $product->setUser($user);
                    $manager->persist($product);
                }
            }

            $manager->flush();
        }
    }
}
