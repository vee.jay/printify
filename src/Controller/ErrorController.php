<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorController extends BaseController
{
    public function show(): JsonResponse
    {
        // somehow catch NotFoundHttpException
        $this->addMessage('Something went wrong.');
        return $this->errorResponse('Request not found');
    }
}
