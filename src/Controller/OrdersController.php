<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrdersController extends BaseController
{
    private $productRepository;
    private $userRepository;
    private $orderRepository;

    public function __construct(ProductRepository $productRepository, UserRepository $userRepository, OrderRepository $orderRepository)
    {
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route("/orders/{id}", name="order_details", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function orderDetails($id): JsonResponse
    {
        if ($item = $this->orderRepository->getOrderById($id)) {
            return $this->successResponse($item);
        }
        return $this->errorResponse('Order not found.');
    }

    /**
     * @Route("/orders", name="place_order", methods={"POST"})
     */
    public function placeOrder(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $user = $this->userRepository->find($request->get('user_id', 0));
        if (!$user) {
            $this->addMessage('User not found.');
            return $this->errorResponse('Order not placed.');
        }

        $item = new Order();
        $item->setUser($user);
        $item->setFullName($request->get('full_name', $user->getFullName()));
        $item->setCountry($request->get('country', ''));
        $item->setPhone($request->get('phone', ''));
        $item->setStatus('new');

        foreach ($request->get('products', []) as $productId) {
            if ($product = $this->productRepository->find($productId)) {
                $item->addProduct($product);
            }
        }

        if ($item->getProduct()->count() == 0) {
            $this->addMessage('No products found.');
            return $this->errorResponse('Order not placed.');
        }

        if ($item->getCountry() == 'US') {
            // domestic orders
            $item->setStreet($request->get('street', ''));
            $item->setState($request->get('state', ''));
            $item->setZip($request->get('zip', ''));
            $item->setExpressShipping((int)$request->get('express', 0));

            $this->checkMandatoryFields(['street', 'state', 'zip'], $request);
        } else {
            // international
            $item->setAddress($request->get('address', ''));
            $item->setState($request->get('state', ''));
            $item->setZip($request->get('zip', ''));
            $item->setExpressShipping(0);

            $this->checkMandatoryFields(['address'], $request);
        }

        $errors = $validator->validate($item);
        $this->addValidatorErrors($errors);

        if ($this->hasErrors()) {
            return $this->errorResponse();
        }

        $cost = $this->orderRepository->countCost($item);
        $item->setCost($cost);

        if ($cost > $user->getBalance()) {
            $this->addMessage('Insufficient funds.');
            return $this->errorResponse('Order not placed.');
        }

        $this->orderRepository->placeOrder($item);

        return $this->successResponse($item->toArray());
    }

    private function checkMandatoryFields(array $fields, Request $request)
    {
        foreach ($fields as $field) {
            if (empty($request->get($field, ''))) {
                $this->addMessage("The $field field can not be empty.");
            }
        }
    }
}
