<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductsController extends BaseController
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/products/{id}", name="product_remove", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function remove($id): JsonResponse
    {
        if ($item = $this->productRepository->find($id)) {
            $this->productRepository->removeProduct($item);
            return $this->successResponse('');
        }
        return $this->errorResponse('Product not found.');
    }

    /**
     * @Route("/products", name="product_add", methods={"POST"})
     */
    public function add(Request $request, ValidatorInterface $validator, UserRepository $userRepository): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($checkUser = $this->productRepository->findOneBy(['sku' => $request->get('sku')])) {
            return $this->errorResponse('Product with selected sku already exists.');
        }

        $user = $userRepository->find($request->get('user_id', 0));
        if (!$user) {
            return $this->errorResponse('User not found. Can not add product for selected user.');
        }

        $item = new Product();
        $item->setTitle($request->get('title', ''));
        $item->setSku($request->get('sku', ''));
        $item->setType($request->get('type', ''));
        $item->setCost($request->get('cost', 0));
        $item->setUser($user);

        $errors = $validator->validate($item);
        $this->addValidatorErrors($errors);

        if ($this->hasErrors()) {
            return $this->errorResponse();
        }

        $entityManager->persist($item);
        $entityManager->flush();

        return $this->successResponse($item->getId());
    }
}
