<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UsersController extends BaseController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/users/{userId}", name="users_list", methods={"GET"}, requirements={"userId"="\d+"})
     */
    public function list(Request $request, ?int $userId = null): JsonResponse
    {
        if ($userId != null) {
            if ($user = $this->userRepository->getUserById($userId)) {
                return $this->successResponse($user);
            }
            return $this->errorResponse('User not found.');
        }

        $result = $this->userRepository->getAllUsers($request->get('email', null));
        if ($result) {
            return $this->successResponse($result);
        }
        return $this->errorResponse('No users not found.');
    }

    /**
     * @Route("/users/{id}", name="user_remove", methods={"DELETE"}, requirements={"id"="\d+"})
     */
    public function remove($id): JsonResponse
    {
        if ($user = $this->userRepository->find($id)) {
            $this->userRepository->removeUser($user);
            return $this->successResponse('');
        }
        return $this->errorResponse('User not found.');
    }

    /**
     * @Route("/users", name="user_add", methods={"POST"})
     */
    public function add(Request $request, ValidatorInterface $validator): JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($checkUser = $this->userRepository->findOneBy(['email' => $request->get('email')])) {
            return $this->errorResponse('User with selected email already exists.');
        }

        $user = new User();
        $user->setFullName($request->get('full_name', ''));
        $user->setEmail($request->get('email', ''));
        $user->setBalance(100);

        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            $this->addValidatorErrors($errors);
            return $this->errorResponse();
        }

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->successResponse($user->getId());
    }
}
