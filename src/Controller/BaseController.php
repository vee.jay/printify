<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    protected $messages = [];

    protected function addMessage($error)
    {
        $this->messages[] = $error;
    }

    protected function successResponse($data): JsonResponse
    {
        $result = [
            'success' => true,
            'result' => $data
        ];

        return new JsonResponse($result, Response::HTTP_OK);
    }

    protected function errorResponse($error = ''): JsonResponse
    {
        if ($error !== '') {
            $this->messages[] = $error;
        }

        $result = [
            'success' => false,
            'result' => $this->messages
        ];

        return new JsonResponse($result, Response::HTTP_BAD_REQUEST);
    }

    protected function hasErrors()
    {
        return count($this->messages) > 0;
    }

    protected function addValidatorErrors($errors)
    {
        foreach ($errors as $error) {
            //$error->getPropertyPath()
            $this->addMessage($error->getMessage());
        }
    }
}
