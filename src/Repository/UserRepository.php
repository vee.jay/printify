<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, User::class);
        $this->manager = $manager;
    }

    public function getAllUsers($email = null)
    {
        if ($email) {
            $users = $this->findBy(['email' => $email]);
        }
        else {
            $users = $this->findAll();
        }

        $result = [];
        foreach ($users as $user) {
            $result[] = $this->getUserDataObject($user);
        }

        return $result;
    }

    public function getUserById($userId)
    {
        if ($user = $this->find($userId)) {
            return $this->getUserDataObject($user);
        }
        return false;
    }

    public function removeUser(User $user)
    {
        $this->manager->remove($user);
        $this->manager->flush();
    }

    private function getUserDataObject(User $user)
    {
        $orders = [];
        foreach ($user->getOrders() as $order) {
            $orders[] = $order->toArray();
        }

        $products = [];
        foreach ($user->getProducts() as $product) {
            $products[] = [
                'id' => $product->getId(),
                'name' => $product->getTitle(),
                'sku' => $product->getSku(),
                'type' => $product->getType()
            ];
        }

        return [
            'id' => $user->getId(),
            'full_name' => $user->getFullName(),
            'email' => $user->getEmail(),
            'balance' => $user->getBalance(),
            'products' => $products,
            'orders' => $orders
        ];
    }
}
