<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    const SHIPPING_COSTS = [
        // type => country => type => [first_item, second_item]
        'mug' => [
            'US' => [
                'default' => [2, 1],
                'express' => [10, 10]
            ],
            'World' => [
                'default' => [5, 2.5]
            ]
        ],
        'tshirt' => [
            'US' => [
                'default' => [1, 0.5],
                'express' => [10, 10]
            ],
            'World' => [
                'default' => [3, 1.5]
            ]
        ]
    ];

    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Order::class);
        $this->manager = $manager;
    }

    public function getOrderById($id)
    {
        if ($order = $this->find($id)) {
            return $order->toArray();
        }
        return false;
    }

    public function placeOrder(Order $order)
    {
        $user = $order->getUser();

        // deduct funds
        $user->setBalance($user->getBalance() - $order->getCost());
        $this->manager->persist($user);
        $this->manager->flush();

        // place the order
        $this->manager->persist($order);
        $this->manager->flush();
    }

    public function countCost(Order $order)
    {
        $orderedItems = [];

        $cost = 0;

        $shippingType = (bool)$order->getExpressShipping() ? 'express' : 'default';

        foreach ($order->getProduct() as $product) {
            // shipping cost
            if (isset(self::SHIPPING_COSTS[$product->getType()][$order->getCountry()][$shippingType])) {
                if (!in_array($product->getId(), $orderedItems)) {
                    // first if more expensive
                    $cost += self::SHIPPING_COSTS[$product->getType()][$order->getCountry()][$shippingType][0];
                } else {
                    // all next items
                    $cost += self::SHIPPING_COSTS[$product->getType()][$order->getCountry()][$shippingType][1];
                }
            }

            $cost += $product->getCost();

            $orderedItems[] = $product->getId();
        }

        return $cost;
    }
}
