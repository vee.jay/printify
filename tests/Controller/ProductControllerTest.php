<?php

namespace App\Tests\Controller;

class ProductControllerTest extends TestCase
{
    public function testCreateProductForNotValidUser()
    {
        $client = static::createClient();

        $client->request('POST', '/products', [
            'title' => 'Some MUG',
            'user_id' => 999999
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);
        $this->assertEquals("User not found. Can not add product for selected user.", $response->result[0]);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCreateProductUserNotFound()
    {
        $client = static::createClient();

        $client->request('POST', '/products');

        $response = $client->getResponse()->getContent();
        $this->assertJson($response);

        $response = json_decode($response);
        $this->assertEquals("User not found. Can not add product for selected user.", $response->result[0]);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCreateProductRequiredFields()
    {
        $client = static::createClient();

        $client->request('POST', '/products', [
            'user_id' => 1
        ]);
        $response = $client->getResponse()->getContent();
        $this->assertJson($response);
        $response = json_decode($response);

        foreach ($this->checkRequiredFieldsArray(['title']) as $msg) {
            $this->assertTrue(in_array($msg, $response->result));
        }

        $this->assertEquals(400, $client->getResponse()->getStatusCode());

        // check for SKU limit longer than 50
        $client->request('POST', '/products', [
            'user_id' => 1,
            'sku' => 'DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD',
            'type' => 'wrong'
        ]);
        $response = $client->getResponse()->getContent();
        $this->assertJson($response);
        $response = json_decode($response);

        $this->assertEquals([
            "The title field can not be empty.",
            "The sku field cannot be longer than 50 characters",
            "The field type must be defined as 'mug' or 'tshirt'",
            "Product cost must be more than 0"
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCreateProduct()
    {
        $client = static::createClient();

        $client->request('POST', '/products', [
            'title' => 'Some MUG',
            'cost' => 1.11,
            'type' => 'mug',
            'sku' => 'XX-555555-DD',
            'user_id' => 1
        ]);

        $response = $client->getResponse()->getContent();
        $this->assertJson($response);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateProductWithSameSku()
    {
        $client = static::createClient();

        $client->request('POST', '/products', [
            'title' => 'Some MUG 2',
            'cost' => 1.11,
            'type' => 'mug',
            'sku' => 'XX-555555-DD',
            'user_id' => 1
        ]);

        $response = $client->getResponse()->getContent();
        $this->assertJson($response);
        $response = json_decode($response);

        $this->assertEquals("Product with selected sku already exists.", $response->result[0]);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testDeleteNonExistentProduct()
    {
        $client = static::createClient();

        $client->request('DELETE', '/products/9999999');
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);
        $this->assertEquals('Product not found.', $response->result[0]);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testDeleteProduct()
    {
        $client = static::createClient();

        $client->request('GET', '/users/1');
        $jsonResponse = $client->getResponse()->getContent();
        $response = json_decode($jsonResponse);

        $client->request('DELETE', '/products/' . $response->result->products[0]->id);

        $response = $client->getResponse()->getContent();
        $this->assertJson($response);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
