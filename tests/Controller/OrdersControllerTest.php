<?php

namespace App\Tests\Controller;

use Faker\Factory;

class OrdersControllerTest extends TestCase
{
    const UserStartBalance = 100;

    // TODO: Test for correct calculation of shipping costs, including domestic express

    public function testPlaceOrderUserNotFound()
    {
        $client = static::createClient();

        $client->request('POST', '/orders', [
            'user_id' => 999999
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $response = json_decode($jsonResponse);

        $this->assertEquals([
            'User not found.',
            'Order not placed.'
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testPlaceOrderNoProducts()
    {
        $client = static::createClient();

        $client->request('POST', '/orders', [
            'user_id' => 2
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $response = json_decode($jsonResponse);

        $this->assertEquals([
            'No products found.',
            'Order not placed.'
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testPlaceOrderValidateFields()
    {
        $client = static::createClient();

        $client->request('POST', '/orders', [
            'user_id' => 2,
            'products' => [1, 2, 3]
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $response = json_decode($jsonResponse);

        $this->assertEquals([
            'The address field can not be empty.',
            "The field status must be defined as 'World' or 'US'",
            'The phone field can not be empty.'
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testPlaceDomesticOrderValidations()
    {
        $client = static::createClient();

        $client->request('POST', '/orders', [
            'user_id' => 2,
            'country' => 'US',
            'products' => [1, 2, 3]
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $response = json_decode($jsonResponse);

        $this->assertEquals([
            'The street field can not be empty.',
            'The state field can not be empty.',
            "The zip field can not be empty.",
            'The phone field can not be empty.'
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testPlaceIntOrderValidations()
    {
        $client = static::createClient();

        $client->request('POST', '/orders', [
            'user_id' => 2,
            'country' => 'World',
            'products' => [1, 2, 3]
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $response = json_decode($jsonResponse);

        $this->assertEquals([
            'The address field can not be empty.',
            'The phone field can not be empty.'
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testPlaceDomesticOrder()
    {
        $client = static::createClient();

        $userId = $this->createUser($client);
        $this->assertTrue((bool)$userId);

        $client->request('POST', '/orders', [
            'user_id' => $userId,
            'country' => 'US',
            'products' => [1, 1, 2, 4],
            'street' => 'Some Street Name',
            'state' => 'NJ',
            'zip' => 'NJ4433',
            'phone' => '18002283338'
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check if balance correct
        $this->assertEquals(self::UserStartBalance - $response->result->cost, $response->result->user->balance);
    }

    public function testPlaceDomesticExpressOrder()
    {
        $client = static::createClient();

        $userId = $this->createUser($client);
        $this->assertTrue((bool)$userId);

        $client->request('POST', '/orders', [
            'user_id' => $userId,
            'country' => 'US',
            'products' => [1, 1, 2, 4],
            'street' => 'Some Street Name',
            'state' => 'NJ',
            'zip' => 'NJ4433',
            'phone' => '18002283338',
            'express' => 1
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check if balance correct
        $this->assertEquals(self::UserStartBalance - $response->result->cost, $response->result->user->balance);
    }

    public function testPlaceInternationalOrder()
    {
        $client = static::createClient();

        $userId = $this->createUser($client);
        $this->assertTrue((bool)$userId);

        $client->request('POST', '/orders', [
            'user_id' => $userId,
            'country' => 'World',
            'products' => [1, 1, 2, 4],
            'address' => 'Latvia, New Street name 44-34, 18999',
            'phone' => '00371444222333'
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check if balance correct
        $this->assertEquals(self::UserStartBalance - $response->result->cost, $response->result->user->balance);
    }

    public function testPlaceOrderInsufficientFunds()
    {
        $client = static::createClient();

        $client->request('POST', '/orders', [
            'user_id' => 1,
            'country' => 'US',
            'products' => [2, 2, 1, 3, 4, 5, 6, 1, 2, 7, 8, 3, 3, 2, 1, 1, 1, 1, 1],
            'street' => 'Some Street Name 2',
            'state' => 'NY',
            'zip' => 'NY2224',
            'phone' => '180940498590',
            'express' => true
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);

        $this->assertEquals([
            'Insufficient funds.',
            'Order not placed.'
        ], $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    private function createUser($client)
    {
        $faker = Factory::create();
        $client->request('POST', '/users', [
            'full_name' => 'Mega Tester Orders',
            'email' => $faker->email
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $userResponse = json_decode($jsonResponse);
        return is_numeric($userResponse->result) ? $userResponse->result : false;
    }
}
