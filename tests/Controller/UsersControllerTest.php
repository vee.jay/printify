<?php

namespace App\Tests\Controller;

class UsersControllerTest extends TestCase
{
    public function testCreateUsers()
    {
        $client = static::createClient();

        $client->request('POST', '/users', [
            'full_name' => 'Mega Tester',
            'email' => 'iam.tester@gmail.com'
        ]);
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetUsers()
    {
        $client = static::createClient();

        $client->request('GET', '/users');

        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $response = json_decode($jsonResponse);
        $this->assertTrue(is_array($response->result));
    }

    public function testGetUserByEmail()
    {
        $client = static::createClient();

        $client->request('GET', '/users', ['email' => 'iam.tester@gmail.com']);

        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // check balance
        $response = json_decode($jsonResponse);
        $this->assertEquals(1, count($response->result));
    }

    public function testCreateFailRequiredFields()
    {
        $client = static::createClient();

        $client->request('POST', '/users');

        $response = $client->getResponse()->getContent();
        $this->assertJson($response);

        $response = json_decode($response);

        $this->assertEquals($this->checkRequiredFieldsArray(['full_name', 'email']), $response->result);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testDeleteNonExistentUser()
    {
        $client = static::createClient();

        $client->request('DELETE', '/users/9999999');
        $jsonResponse = $client->getResponse()->getContent();
        $this->assertJson($jsonResponse);

        $response = json_decode($jsonResponse);
        $this->assertEquals('User not found.', $response->result[0]);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testDeleteUser()
    {
        $client = static::createClient();

        $client->request('GET', '/users', ['email' => 'iam.tester@gmail.com']);
        $jsonResponse = $client->getResponse()->getContent();
        $response = json_decode($jsonResponse);

        $client->request('DELETE', '/users/'.$response->result[0]->id);

        $response = $client->getResponse()->getContent();
        $this->assertJson($response);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
