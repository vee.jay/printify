<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestCase extends WebTestCase
{
    function checkRequiredFieldsArray($items = []): array
    {
        return array_map(function ($item) {
            return "The $item field can not be empty.";
        }, $items);
    }
}
